System.register([], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var DictionaryItem;
    return {
        setters: [],
        execute: function () {
            DictionaryItem = class DictionaryItem {
                constructor(key, value) {
                    this.key = key;
                    this.value = value;
                }
            };
            exports_1("DictionaryItem", DictionaryItem);
        }
    };
});
//# sourceMappingURL=DictionaryItem.js.map