System.register(["./DictionaryItem"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var DictionaryItem_1, Dictionary;
    return {
        setters: [
            function (DictionaryItem_1_1) {
                DictionaryItem_1 = DictionaryItem_1_1;
            }
        ],
        execute: function () {
            Dictionary = class Dictionary {
                constructor() {
                    this._keys = new Array();
                    this._list = new Array();
                }
                Add(t, r) {
                    let item = new DictionaryItem_1.DictionaryItem(t, r);
                    this.addItemToList(item);
                }
                Remove(t, r) {
                    let item = new DictionaryItem_1.DictionaryItem(t, r);
                    this.removeItemFromList(item);
                }
                Find(t) {
                    let result = null;
                    this._list.forEach((item) => {
                        if (item.key === t) {
                            result = item.value;
                        }
                    });
                    return result;
                }
                addItemToList(data) {
                    try {
                        if (!this.isKeyUsed(data.key)) {
                            this._list.push(data);
                            this.List = this._list;
                        }
                    }
                    catch (e) {
                        console.log(e);
                    }
                }
                isKeysEmpty(key) {
                    if (this._keys.length === 0) {
                        return true;
                    }
                    return false;
                }
                isKeyUsed(key) {
                    if (this.isKeysEmpty(key) ||
                        this._keys.indexOf(key) === -1) {
                        this._keys.push(key);
                        return false;
                    }
                    console.log(`${key} is already used!`);
                    throw `${key} is already used!`;
                }
                removeItemFromList(item) {
                    // Find Index of Item
                    var index = this.findByItem(item);
                    if (index === -1) {
                        console.log(`${item.key} is not in the list`);
                    }
                    try {
                        this._list.splice(index, 1);
                        this.List = this._list;
                    }
                    catch (e) {
                        console.error(`Error removing Item from List`);
                    }
                }
                findByItem(item) {
                    let index = 0;
                    this._list.forEach((x) => {
                        if (x.key === item.key && x.value === item.value) {
                            return index;
                        }
                        index++;
                    });
                    return index - this._list.length + 1;
                }
            };
            exports_1("Dictionary", Dictionary);
        }
    };
});
//# sourceMappingURL=Dictionary.js.map